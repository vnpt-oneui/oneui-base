import { registerApplication, start } from "single-spa";
import {
  constructApplications,
  constructRoutes,
  constructLayoutEngine,
} from "single-spa-layout";

registerApplication({
  name: "start",
  app: () => {
    return System.import("start");
  },
  activeWhen: "/",
  customProps: {
    some: "value",
  },
});

start();

window.addEventListener("system:config:loaded", (event) => {
  const appConfigs = event.detail.appConfigs;

  const routesConfig = document.querySelector("#single-spa-layout");
  routesConfig.innerHTML =
    `<single-spa-router>` + event.detail.appLayout + `</single-spa-router>`;
  const routes = constructRoutes(routesConfig);
  const applications = constructApplications({
    routes,
    loadApp({ name }) {
      return System.import(appConfigs[name]);
    },
  });
  const layoutEngine = constructLayoutEngine({ routes, applications });

  applications.forEach(registerApplication);
  layoutEngine.activate();
  start();

  setTimeout(() => {
    const event2 = new CustomEvent("system:init:success", {
      bubbles: true,
      detail: {},
    });
    window.dispatchEvent(event2);
  }, 0);
});
