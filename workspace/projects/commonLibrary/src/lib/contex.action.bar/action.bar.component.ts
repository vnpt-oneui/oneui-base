import {
  AfterViewInit,
  Component,
  ElementRef,
  HostListener,
  Input,
  OnInit,
  Renderer2,
  TemplateRef,
  ViewChild,
} from '@angular/core';

@Component({
  selector: 'contexual-actionbar',
  templateUrl: './action.bar.component.html',
  styleUrls: ['./action.bar.component.css'],
})
export class ContexualActionBarComponent implements OnInit, AfterViewInit {
  @Input() left: TemplateRef<void>;
  @Input() right: TemplateRef<void>;
  @Input() fixAtTop: number = 85;
  @Input() bgColor: string;

  @ViewChild('actionbar') public actionbar: ElementRef;
  @ViewChild('actionbarwr') public actionbarwr: ElementRef;

  @HostListener('window:resize', ['$event'])
  onResize(event) {
    setTimeout(() => {
      this.renderer.setStyle(
        this.actionbarwr.nativeElement,
        'width',
        this.actionbar.nativeElement.offsetWidth + 'px'
      );
      this.renderer.setStyle(
        this.actionbarwr.nativeElement,
        'top',
        this.fixAtTop + 'px'
      );
    }, 200);
  }
  constructor(private renderer: Renderer2) {}

  ngOnInit(): void {}

  close(): void {}

  ngAfterViewInit() {
    setTimeout(() => {
      this.renderer.setStyle(
        this.actionbarwr.nativeElement,
        'width',
        this.actionbar.nativeElement.offsetWidth + 'px'
      );
      this.renderer.setStyle(
        this.actionbarwr.nativeElement,
        'top',
        this.fixAtTop + 'px'
      );
    }, 200);
  }
}

