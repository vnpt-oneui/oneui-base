import { Component } from '@angular/core';
import { QuickMenuModel } from './model/appmodel';

@Component({
  selector: 'quickmenu-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent {
  title = 'quickmenu';
  menuConfig: QuickMenuModel[] = [
    {
      routerLink: '/home',
      iconName: 'dashboard.png',
      nameMenu: 'Trang chủ',
    },
    {
      routerLink: '/home/settings',
      iconName: 'settings.svg',
      nameMenu: 'Setting',
    },
  ];
}
