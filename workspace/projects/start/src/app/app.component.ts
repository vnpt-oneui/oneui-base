import {
  AfterViewChecked,
  AfterViewInit,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { Base } from '@vnpt/oneui-core';

@Component({
  selector: 'app-start',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, AfterViewInit {
  loadingScreen: boolean = true;
  appConfigs: any = {};
  appLayout: string;
  errMsg: string;

  @ViewChild('routerConfig') routerConfig: ElementRef;

  constructor() {}

  ngOnInit() {}

  ngAfterViewInit() {
    this.initConfig();
    //listening system init success
    Base.registerEvent('system:init:success', (event: any) => {
      this.routerConfig.nativeElement.innerHTML = '';
      setTimeout(() => {
        this.loadingScreen = false;
      }, 100);
    });
  }

  initConfig() {
    this.appConfigs = {
      header: 'http://localhost:4201/main.js',
      login: 'http://localhost:4202/main.js',
      breadcrumb: 'http://localhost:4203/main.js',
      emptypage: 'http://localhost:4204/main.js',
      errorpage: 'http://localhost:4205/main.js',
      quickmenu: 'http://localhost:4213/main.js',
      nopermission: 'http://localhost:4209/main.js',
      notfound: 'http://localhost:4210/main.js',
      settings: 'http://localhost:4214/main.js',
    };

    this.appLayout = this.routerConfig.nativeElement.innerHTML;
    Base.sendEvent('system:config:loaded', {
      appConfigs: this.appConfigs,
      appLayout: this.appLayout,
    });
  }
}
